#include "library.h"

void sun_moon(const int option)
{
	const double x = get_window_width() / 2;
	const double y = get_window_height() / 4;

	if (option == 1)
		set_pen_color(color::yellow);
	else
		set_pen_color(color::grey);
	set_pen_width(75);
	move_to(x, y);
	draw_point();
}

void window(const int x, const int y, const int width, const int height, double red, double green, double blue)
{
	fill_rectangle(x, y, width, height, color::rgb(red, green, blue));
}

void row_windows(const int num_windows, const int height, const int width, const int x, const int y, double red, double green, double blue)
{
	for (int i = 0; i < num_windows; i++)
	{
		window(x + (5 * width / 4 * i), y, width, height, red, green, blue);
	}
}

void block_windows(const int floors, const int num_windows, const int height, const int width, const int x, const int y, double red, double green, double blue)
{
	for (int i = 0; i < floors; i++)
	{
		row_windows(num_windows, height, width, x, y + (i * 5 * height / 4), red, green, blue);
	}
}

void office(const int x, const int y, const int width)
{
	const int height = random_in_range(get_window_height() / 3, 3 * get_window_height() / 4);
	const double win_height = random_in_range(height / 8, height / 4);
	const double win_width = random_in_range(width / 8, width / 4);
	const int floors = height / (win_height*1.3);
	const int num_win_row = width / (win_width*1.31);

	const double win_y = y - height + win_height / 2.0;
	const double win_x = x + win_width - win_width / 2.0;

	const double red_b = random_in_range(1, 10) / 10.0;
	const double green_b = random_in_range(1, 10) / 10.0;
	const double blue_b = random_in_range(1, 10 / 10.0);
	fill_rectangle(x, y - height, width, height, color::rgb(red_b, green_b, blue_b));

	const double red_w = random_in_range(1, 10) / 10.0;
	const double green_w = random_in_range(1, 10) / 10.0;
	const double blue_w = random_in_range(1, 10 / 10.0);
	block_windows(floors, num_win_row, win_height, win_width, win_x, win_y, red_w, green_w, blue_w);
}

void roof(const int x, const int y, const int height, const int width)
{
	set_pen_width(1);
	const double red_r = random_in_range(35.2, 50) / 100.0;
	const double green_r = random_in_range(11.4, 25) / 100.0;
	const double blue_r = random_in_range(0, 14) / 100.0;
	move_to(x - 20, y - height);
	start_shape();
	draw_to(x + width / 2, y - height - 50);
	note_position();
	draw_to(x + width + 20, y - height);
	note_position();
	draw_to(x - 20, y - height);
	note_position();
	fill_shape(color::rgb(red_r, green_r, blue_r));
}

void house(const int x, const int y, const int width)
{
	const int height = random_in_range(50, 150);
	const double win_height = random_in_range(height / 5, height / 3);
	const double win_width = random_in_range(width / 8, width / 4);

	const double red_h = random_in_range(84.3, 96.8) / 100.0;
	const double green_h = random_in_range(86.2, 100.0) / 100.0;
	const double blue_h = random_in_range(62, 75) / 100.0;

	fill_rectangle(x, y - height, width, height, color::rgb(red_h, green_h, blue_h));

	const double red_w = random_in_range(0, 34) / 100.0;
	const double green_w = random_in_range(0, 78.4) / 100.0;
	const double blue_w = random_in_range(67, 100) / 100.0;

	const double space_y = (height - (2 * win_height)) / 3;
	const double win_y1 = y - height + space_y;
	const double win_y2 = y - height + (2 * space_y) + win_height;

	const double space_x = (width - (3 * win_width)) / 4;
	const double win_x1 = x + space_x;
	const double win_x2 = x + (2 * space_x) + win_width;
	const double win_x3 = x + (3 * space_x) + (2 * win_width);

	const int door = random_in_range(1, 3);
	const double door_height = 2 * height / 5;
	const double door_width = 3 * win_width / 4;
	const double red_d = random_in_range(1, 10) / 10.0;
	const double green_d = random_in_range(1, 10) / 10.0;
	const double blue_d = random_in_range(1, 10) / 10.0;

	if (height > 100)
	{
		window(win_x1, win_y1, win_width, win_height, red_w, green_w, blue_w);
		window(win_x2, win_y1, win_width, win_height, red_w, green_w, blue_w);
		window(win_x3, win_y1, win_width, win_height, red_w, green_w, blue_w);
	}
	if (door == 1)
	{
		fill_rectangle(win_x1, y - door_height, door_width, door_height, color::rgb(red_d, green_d, blue_d));
		window(win_x2, win_y2, win_width, win_height, red_w, green_w, blue_w);
		window(win_x3, win_y2, win_width, win_height, red_w, green_w, blue_w);
	}
	else if (door == 2)
	{
		fill_rectangle(win_x2, y - door_height, door_width, door_height, color::rgb(red_d, green_d, blue_d));
		window(win_x1, win_y2, win_width, win_height, red_w, green_w, blue_w);
		window(win_x3, win_y2, win_width, win_height, red_w, green_w, blue_w);
	}
	else
	{
		fill_rectangle(win_x3, y - door_height, door_width, door_height, color::rgb(red_d, green_d, blue_d));
		window(win_x1, win_y2, win_width, win_height, red_w, green_w, blue_w);
		window(win_x2, win_y2, win_width, win_height, red_w, green_w, blue_w);
	}
	roof(x, y, height, width);
}

void tree(const int a)
{
	int count = 0;
	const int bottom_x = get_window_height() - get_window_height() / 25;
	const int trunk_width = random_in_range(20, 80);
	const int trunk_height = random_in_range(40, 100);
	const int leaves = random_in_range(10, 40);
	const int leaves_height = random_in_range(20, 80);
	const int leaves_width = (leaves * 2) + trunk_width;
	const int tree_height = trunk_height + leaves_height;

	set_pen_color(color::brown);
	fill_rectangle(a + leaves, bottom_x - trunk_height, trunk_width, trunk_height);
	set_pen_color(color::green);
	while (count < 600)
	{
		int x = random_in_range(a, a + leaves_width);
		int y = random_in_range(bottom_x - tree_height, bottom_x - trunk_height);
		move_to(x, y);
		set_pen_width(5);
		draw_point();
		count++;
	}
	set_pen_color(color::white);

}


void grass()
{
	const int window_height = get_window_height();
	const int height = window_height - window_height / 25;
	const int increment = window_height / 25;

	const double red = random_in_range(11.9, 41) / 100.0;
	const double green = random_in_range(70.2, 100.0) / 100.0;
	const double blue = random_in_range(11, 45.5) / 100.0;
	fill_rectangle(0, height, get_window_width(), get_window_height(), color::rgb(red, green, blue));
	for (int i = height; i < window_height; i += increment)
	{

	}
	
}

void lightpost(const int pos)
{
	int position = get_window_height() - get_window_height() / 25 - get_window_height() / 2;
	set_pen_color(color::grey);
	fill_rectangle(pos, position, 20, get_window_height() / 2, color::grey);
	move_to(pos + 15, position + 5);
	set_pen_width(30);
	set_pen_color(color::white);
	draw_point();
}

void time(const int hour)
{
	const double red = .5 - (.1*(hour / 3));
	const double green = .75 - (.1*(hour / 3));
	const double blue = 1 - (0.1*(hour / 3));
	const int height = get_window_height() - get_window_height() / 25;
	if (hour <= 6 || hour >= 20)
	{
		fill_rectangle(0, 0, get_window_width(), height, color::rgb(0.1, 0.15, 0.25));
		sun_moon(0);
	}
	else
	{
		fill_rectangle(0, 0, get_window_width(), height, color::rgb(red, green, blue));
		sun_moon(1);
	}
}

void background()
{

	const int hour = random_in_range(1, 24);
	time(hour);
	grass();
	lightpost(20);
	lightpost(950);
	lightpost(260);
}

void main()
{
	make_window(1000, 600);
	background();
	office(50, get_window_height(), 200);
	office(300, get_window_height(), 75);
	tree(600);
	house(400, get_window_height(), 150);
	house(800, get_window_height(), 100);

}